import tomli
import argparse
from _create_control_launch import create_launchfile

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-c","--config", help="Config file", default="../config/default_control_config.toml")

    args = parser.parse_args()

    with open(args.config, encoding="utf-8") as f:
        toml_dict = tomli.load(f)

    create_launchfile(toml_dict)

if __name__ == "__main__":
    main()
