import tomli

def create_launchfile(tomli_dict):
    f = open("../launch/generated/" + tomli_dict['simulation']['name'] + ".launch", "w+")



    f.write("<?xml version=\"1.0\"?>\n\
<launch>\n\
\n\
")

    for item in tomli_dict:
        if tomli_dict[item]['type'] == "sim description":
            print("Generating " + tomli_dict[item]['name'] + " launch file")
        elif tomli_dict[item]['type'] == "simple robot":
            robotid = str(tomli_dict[item]['robotid'])
            file_path = tomli_dict[item]['file_path']
            metricpath = str(tomli_dict[item]['metric_path'])
            f.write("\
    <group ns=\"robot" + robotid +"\">\n\
        <param name=\"filename\" value=\"$(find send_multiple_paths)/" + file_path + "\"/>\n\
        <param name=\"robotid\" value=\"" + robotid + "\"/>\n\
        <param name=\"metricpath\" value=\"" + metricpath + "\"/>\n\
        <node name=\"robot" + robotid + "_path_viewer\" pkg=\"send_multiple_paths\" type=\"path_visual_publisher.py\">\n\
        </node>\n\
        <node name=\"robot" + robotid + "_goals_publisher\" pkg=\"send_multiple_paths\" type=\"goal_publisher.py\">\n\
        </node>\n\
    </group>\n\
\n")
        else:
            print("Robot " + tomli_dict[item]['type'] + " not supported")

    f.write("\
</launch>\n\
\n")

    f.close()
    print("Created")
    return
