# control-container
Here are all packages referred to the control of robots.


## Send paths to multiple robots
### How to send paths to the robots

1. Generate the launchfile file that launch the nodes with the trajectories indicated in the configuration file. From a terminal in the `local computer` execute:

```bash
python3 control_manager.py
```

2. Once the launchfile file is generated, we can send the trajectories to the robots by running the following command inside the `control container`:

```bash
$ docker start control-simulation
$ docker exec -it control-simulation bash
root@kelsey:/home/catkin_ws# roslaunch send_multiple_paths path_goal_publisher.launch
```

### Configuration
The toml settings file must be modified in the `send_multiple_paths/config` folder or the file indicated with the `-c` option as follows:
```bash
python3 control_manager.py
```
The default configuration file is located at `send_multiple_paths/config/default_control_config.toml`. We can specify the coordinates by which we want the robots go.

#### toml configuration file

```yaml

[simulation]
name = "send_path_3_robots"
type = "sim description"

[robot0]
robotid = 0
type = "simple robot"
file_path = "../paths/sample_path.txt"
metric_path = true

[robot1]
robotid = 1
type = "simple robot"
file_path = "../paths/simple_path.txt"
metric_path = true
```

- [**simulation**]: configuration specific label
  - **name**: name of the launchfile file to generate
  - **type**: indicate that it is the descriptions of the simulation
- [**robot0**]: label to specify the robot driver settings
   - **robotid**: robot ID. Also used to configure the namesmace and topics if `simple_robot` is selected
   - **file_path**: File that contains the different coords by which we want the robot to go. See below for more details of the file format.
   - **metric_path**: to indicate whether the coordinates are in meters. In case of false, the coordinates are on pixel on the map used for navigation.


### Robot coord file
The default file is located in `paths/simple_path.txt` and is made up of a list of `{x y YAW}` position separated by tabs:

```txt
x	y	Y
0.0	0.0	0.0
2.0	0.0	0.0
2.0	2.0	0.0
0.0	2.0	0.0
0.0	0.0	0.0
```
