import os
import platform
import sys
import argparse
import shutil
import csv

def create_simple_robots_instances(sim_name, simulation_type, robot_count):

    for robot_id in range(0, int(robot_count)):
        laser_enable = True
        rgbd_enable = True

        #--------------------create robot components file--------------------
        template_launchfile = "../../simple_robot_description/launch/spawn_simple_robot.launch"
        create_robot_components_launchfile(template_launchfile, sim_name, simulation_type, robot_id, laser_enable, rgbd_enable)

    return True

def create_simple_robots_instances_from_dict(tomli_dict):

    sim_name = tomli_dict['simulation']['sim_name']
    map_name = tomli_dict['simulation']['map_name']
    simulation_type = tomli_dict['simulation']['simulation_type']
    robot_count = 0

    for item in tomli_dict:
        if tomli_dict[item]['type'] == "simple robot":
            robot_id = tomli_dict[item]['robotid']
            laser_enable = tomli_dict[item]['laser_enable']
            rgbd_enable = tomli_dict[item]['rgbd_enable']

            #--------------------create robot components file--------------------
            template_launchfile = "../../simple_robot_description/launch/spawn_simple_robot.launch"
            create_robot_components_launchfile(template_launchfile, sim_name, simulation_type, robot_id, laser_enable, rgbd_enable)
        elif tomli_dict[item]['type'] == "romaa":
            robot_id = tomli_dict[item]['robotid']
            laser_enable = tomli_dict[item]['laser_enable']
            rgbd_enable = tomli_dict[item]['rgbd_enable']

            #--------------------create robot components file--------------------
            template_launchfile = "../launch/extern/spawn_romaa.launch"
            create_robot_components_launchfile(template_launchfile, sim_name, simulation_type, robot_id, laser_enable, rgbd_enable)
        elif tomli_dict[item]['type'] == "husky":
            robot_id = tomli_dict[item]['robotid']
            laser_enable = tomli_dict[item]['laser_enable']
            rgbd_enable = tomli_dict[item]['rgbd_enable']

            #--------------------create robot components file--------------------
            template_launchfile = "../launch/extern/spawn_husky.launch"
            create_robot_components_launchfile(template_launchfile, sim_name, simulation_type, robot_id, laser_enable, rgbd_enable)


        else:
            if tomli_dict[item]['type'] != "sim description":
                print("ERROR: robot type \"" + str(tomli_dict[item]['type']) + "\" not supported")

    return True

def create_robot_components_launchfile(template_launchfile, sim_name, simulation_type, robot_id, laser_enable, rgbd_enable):
    f_original = open(template_launchfile, "r+")
    f_generated = open("../launch/generated/" + sim_name + "/robot" + str(robot_id) + "_" + str(simulation_type) + ".launch", "w+")

    #read file
    file_source = f_original.read()

    replace_string = file_source.replace('<!-- LASER_COFIGURATION -->', \
            get_simple_robot_laser_configuration(laser_enable))
    replace_string = replace_string.replace('<!-- REALSENSE_COFIGURATION -->', \
            get_simple_robot_realsense_configuration(rgbd_enable))
    replace_string = replace_string.replace('<!-- ROBOT_STATE_PUBLISHER -->', \
            get_robot_state_publisher_config())
    replace_string = replace_string.replace('<!-- ROBOT_FAKE_LOCALIZATION_PUBLISHER -->', \
            get_robot_fake_localization_publisher_config())
    replace_string = replace_string.replace('<!-- MAP2ODOM_TF2PUBLISHER_CONFIG -->', \
            get_tf_map2odom_publisher_config())
    replace_string = replace_string.replace('<!-- NAVIGATION_STACK_CONFIG -->', \
            get_navigation_stack_config(sim_name))
    f_generated.write(replace_string)

    f_original.close()
    f_generated.close()

def get_simple_robot_laser_configuration(laser_enable):
  return "\
  <arg name=\"laser_enabled\" default=\"" + str(laser_enable).lower() + "\"/>\n\
"

def get_simple_robot_realsense_configuration(rgbd_enable):
  return "\
  <arg name=\"realsense_camera_enabled\" default=\"" + str(rgbd_enable).lower() + "\"/>\n\
"

def get_navigation_stack_config(sim_name):
  return "\
<!-- Navigation_stack_config -->\n\
    <node pkg=\"move_base\" type=\"move_base\" respawn=\"false\" name=\"$(arg namespace)_move_base\" output=\"screen\">\n\
    <rosparam file=\"$(find sim_manager)/config/generated/" + str(sim_name) + "/$(arg namespace)_costmap_common_params.yaml\" command=\"load\" ns=\"global_costmap\" />\n\
    <rosparam file=\"$(find sim_manager)/config/generated/" + str(sim_name) + "/$(arg namespace)_costmap_common_params.yaml\" command=\"load\" ns=\"local_costmap\" />\n\
    <rosparam file=\"$(find sim_manager)/config/generated/" + str(sim_name) + "/$(arg namespace)_local_costmap_params.yaml\" command=\"load\" />\n\
    <rosparam file=\"$(find sim_manager)/config/generated/" + str(sim_name) + "/$(arg namespace)_global_costmap_params.yaml\" command=\"load\" />\n\
    <rosparam file=\"$(find sim_manager)/config/generated/" + str(sim_name) + "/$(arg namespace)_base_local_planner_params.yaml\" command=\"load\" />\n\
    <remap from=\"cmd_vel\" to=\"$(arg namespace)/cmd_vel\"/>\n\
    <remap from=\"map\" to=\"/map\"/>\n\
 </node>\n\
"

def get_robot_state_publisher_config():
     return "\
<!-- Robot_state_publisher -->\n\
    <node name=\"joint_state_publisher_$(arg namespace)\" pkg=\"joint_state_publisher\" type=\"joint_state_publisher\">\n\
    <param name=\"use_gui\" value=\"False\"/>\n\
    </node>\n\
\n\
    <node name=\"robot_state_publisher_$(arg namespace)\" pkg=\"robot_state_publisher\" type=\"robot_state_publisher\" />\n\
"

def get_robot_fake_localization_publisher_config():
  return "\
<!-- Robot_fake_localization_publisher -->\n\
    <node pkg=\"fake_localization\" type=\"fake_localization\" name=\"fake_localization\" output=\"screen\">\n\
    <remap from=\"base_pose_ground_truth\" to=\"$(arg namespace)/odom\"/>\n\
    <param name=\"odom_frame_id\" value=\"/$(arg namespace)_tf/odom\"/>\n\
\n\
    <param name=\"base_frame_id\" value=\"/$(arg namespace)_tf/base_link\"/>\n\
    <param name=\"global_frame_id\" value=\"/map\"/>\n\
\n\
  </node>\n\
\n\
"

def get_tf_map2odom_publisher_config():
    return "\
<node pkg=\"tf\" type=\"static_transform_publisher\" name=\"odom_$(arg namespace)_2_map\" args=\"0 0 0 0 0 0 /map /$(arg namespace)_tf/odom 100\"/>\n\
\n\
"
