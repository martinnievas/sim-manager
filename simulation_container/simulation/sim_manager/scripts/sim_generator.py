import os
import platform
import sys
import argparse
import shutil
import tomli
from _create_map import create_map_main
from _create_robots import create_robots_type_launch
from _create_robots import create_robots_type_launch_from_dict
from _describe_robots import create_simple_robots_instances
from _describe_robots import create_simple_robots_instances_from_dict
from _move_base_robots import create_robots_move_base_config
from _move_base_robots import create_robots_move_base_config_from_dict

#  def export_map(world_name):
#      os.system("cp -r ../worlds/" + str(world_name) + " ~/.gazebo/models/")
#      os.system("cp -r ../random_world/media/ ~/.gazebo/models/" + str(world_name) + "/")
#      print("map exported to:  ~/.gazebo/models/" + str(world_name))
#      return

def check_error(error_code):
    """Check error code"""
    if  error_code == False:
        print("Error")
        sys.exit()

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")
    parser.add_argument("-c","--config", help="Config file", default="../config/default_simulation_config.toml")

    parser.add_argument("--robotconfig", help="File with Robot's initial position", default="../config/robot_initial_position.txt")

    args = parser.parse_args()

    with open(args.config, encoding="utf-8") as f:
        toml_dict = tomli.load(f)

    exit_code = create_map_main(\
            toml_dict['simulation']['sim_name'],\
            toml_dict['simulation']['map_path'],\
            toml_dict['simulation']['map_name'],\
            toml_dict['simulation']['simulation_type'],\
            toml_dict['simulation']['robot_count'])
    check_error(exit_code)

    if toml_dict['simulation']['read_initial_pose_from_file'] == True:
        exit_code = create_robots_type_launch(\
                toml_dict['simulation']['sim_name'],\
                toml_dict['simulation']['map_path'],\
                toml_dict['simulation']['map_name'],\
                toml_dict['simulation']['simulation_type'],\
                toml_dict['simulation']['robot_count'],\
                toml_dict['simulation']['robot_initial_pose'],\
                toml_dict['simulation']['random_robot_init'])
        check_error(exit_code)

        exit_code = create_simple_robots_instances(\
                toml_dict['simulation']['sim_name'],\
                toml_dict['simulation']['simulation_type'],\
                toml_dict['simulation']['robot_count'])
        check_error(exit_code)

        exit_code = create_robots_move_base_config(\
                toml_dict['simulation']['sim_name'],\
                toml_dict['simulation']['robot_count'])
        check_error(exit_code)

    else:
        exit_code = create_robots_type_launch_from_dict(toml_dict)
        check_error(exit_code)
        exit_code = create_simple_robots_instances_from_dict(toml_dict)
        check_error(exit_code)
        exit_code = create_robots_move_base_config_from_dict(toml_dict)
        check_error(exit_code)

if __name__ == "__main__":
    main()

