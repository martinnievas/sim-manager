import os
import platform
import sys
import argparse
import shutil
import csv
import random

def read_robot_initial_position(csv_name):
    """Read csv file with the robot's initial position"""
    with open(csv_name, mode='r') as csv_file:
        robot_coords = []
        yaw_coords = []
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            robot_coords.append(float(row["x"]))
            robot_coords.append(float(row["y"]))
            robot_coords.append(float(row["z"]))
            yaw_coords.append(float(row["yaw"]))
        return robot_coords, yaw_coords

def read_robot_random_initial_position(csv_name, robot_count):
    """Read csv file with the robot's initial position"""
    with open(csv_name, mode='r') as csv_file:
        robot_coords = []
        yaw_coords = []
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            robot_coords.append([float(row["x"]), float(row["y"]), float(row["z"])])
            yaw_coords.append(float(row["yaw"]))
        random_robot_coords = random.choices(robot_coords, k = int(robot_count))
        robot_coords_flat = [val for coord in random_robot_coords for val in coord]
        return robot_coords_flat, yaw_coords

def create_robots_type_launch(sim_name, map_path, map_name, simulation_type, robot_count, robot_initial_position, \
        random_robot_position):

    #--------------------create simulation file--------------------
    f = open("../launch/generated/" + sim_name + "/robots_" + str(simulation_type) + ".launch", "w+")
    f.write("<?xml version=\"1.0\"?>\n\
<launch>\n\
\n")

    robot_positions, yaw_angles = read_robot_initial_position(robot_initial_position)
    if len(robot_positions)//3 < int(robot_count):
        print("ERROR robot count is greather than robot position, check " +\
        str(robot_initial_position) + " file")
        return False

    for robot_id in range(0, int(robot_count)):
        f.write("<!-- BEGIN ROBOT " + str(robot_id) + " -->\n\
  <group ns=\"robot" + str(robot_id) + "\">\n\
    <param name=\"tf_prefix\" value=\"robot" + str(robot_id) + "_tf\" />\n\
    <include file=\"$(find sim_manager)/launch/generated/" + str(sim_name) + "/robot" + str(robot_id) + "_" + str(simulation_type) + ".launch\" >\n\
    <arg name=\"init_pose\" value=\"-x " + str(robot_positions[robot_id*3]) + " -y " + str(robot_positions[robot_id*3+1]) + " -z " + str(robot_positions[robot_id*3+2]) + " -Y " + str(yaw_angles[robot_id]) + "\" />\n\
      <arg name=\"namespace\"  value=\"robot" + str(robot_id) + "\" />\n\
    </include>\n\
  </group>\n\
\n")

    f.write("<!-- Map publisher node -->\n\
  <node name=\"map_publisher\" pkg=\"map_server\" type=\"map_server\"\n\
    args=\"$(find world_pkg)/" + str(map_path) + str(map_name) + "\" output=\"screen\" />\n\
\n\
</launch>\n\
")
    f.close()
    print("../launch/generated/" + sim_name + "/robots_" + str(simulation_type) + ".launch" + " created")
    return True

def create_robots_type_launch_from_dict(tomli_dict):

    sim_name = tomli_dict['simulation']['sim_name']
    map_path = tomli_dict['simulation']['map_path']
    map_name = tomli_dict['simulation']['map_name']
    simulation_type = tomli_dict['simulation']['simulation_type']

    #--------------------create simulation file--------------------
    f = open("../launch/generated/" + sim_name + "/robots_" + str(simulation_type) + ".launch", "w+")
    f.write("<?xml version=\"1.0\"?>\n\
<launch>\n\
\n")

    for item in tomli_dict:
        if tomli_dict[item]['type'] == "simple robot" or\
                tomli_dict[item]['type'] == "romaa" or\
                tomli_dict[item]['type'] == "husky":

            robot_id = str(tomli_dict[item]['robotid'])
            robot_position_x = tomli_dict[item]['initial_position'][0]
            robot_position_y = tomli_dict[item]['initial_position'][1]
            robot_position_z = tomli_dict[item]['initial_position'][2]
            robot_yaw_angle = tomli_dict[item]['initial_position'][3]

            f.write("<!-- BEGIN ROBOT " + str(robot_id) + " -->\n\
  <group ns=\"robot" + str(robot_id) + "\">\n\
    <param name=\"tf_prefix\" value=\"robot" + str(robot_id) + "_tf\" />\n\
    <include file=\"$(find sim_manager)/launch/generated/" + str(sim_name) + "/robot" + str(robot_id) + "_" + str(simulation_type) + ".launch\" >\n\
    <arg name=\"init_pose\" value=\"-x " + str(robot_position_x) + " -y " + str(robot_position_y) + " -z " + str(robot_position_z) + " -Y " + str(robot_yaw_angle) + "\" />\n\
      <arg name=\"namespace\"  value=\"robot" + str(robot_id) + "\" />\n\
    </include>\n\
  </group>\n\
\n")

    f.write("<!-- Map publisher node -->\n\
  <node name=\"map_publisher\" pkg=\"map_server\" type=\"map_server\"\n\
    args=\"$(find world_pkg)/" + str(map_path) + str(map_name) + "\" output=\"screen\" />\n\
\n\
</launch>\n\
")
    f.close()
    print("../launch/generated/" + sim_name + "/robots_" + str(simulation_type) + ".launch" + " created")
    return True
