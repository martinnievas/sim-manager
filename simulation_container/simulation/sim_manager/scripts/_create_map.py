import os
import platform
import sys
import argparse
import shutil

def create_map_main(sim_name, map_path, map_name, simulation_type, robot_count):

    try:
        os.makedirs("../launch/generated/" + sim_name)
    except FileExistsError:
        print("File already exist")
        overwrite = input("Do you want to overwrite it? y/n: ")
        if  overwrite == "n":
            print("Abort")
            return False
        elif overwrite == "y":
            shutil.rmtree("../launch/generated/" + sim_name)
            os.makedirs("../launch/generated/" + sim_name)
            #--------------------create main.launchfile file--------------------
            f = open("../launch/generated/" + sim_name + "/generated_main_" + sim_name + ".launch", "w+")
            f.write("<?xml version=\"1.0\"?>\n\
<launch>\n\
  <param name=\"/use_sim_time\" value=\"true\" />\n\
  <arg name=\"world\" default=\"" + str(sim_name) + "\" />\n\
  <arg name=\"map\" default=\"" + str(map_name) + "\" />\n\
  <arg name=\"type\" default=\"" + str(simulation_type) + "\" />\n\
  <arg name=\"debug\" default=\"false\"/>\n\
  <arg name=\"gui\" default=\"false\"/>\n\
  <arg name=\"headless\" default=\"false\"/>\n\
  <arg name=\"pause\" default=\"false\"/>\n\
\n\
  <!-- start world -->\n\
  <include file=\"$(find gazebo_ros)/launch/empty_world.launch\">\n\
    <arg name=\"world_name\" value=\"$(find world_pkg)/" + str(map_path) + "$(arg world).world\"/>\n\
    <arg name=\"debug\" value=\"$(arg debug)\" />\n\
    <arg name=\"gui\" value=\"$(arg gui)\" />\n\
    <arg name=\"paused\" value=\"$(arg pause)\"/>\n\
    <arg name=\"use_sim_time\" value=\"true\"/>\n\
    <arg name=\"headless\" value=\"$(arg headless)\"/>\n\
  </include>\n\
\n\
  <!-- launch all robots -->\n\
  <include file=\"$(find sim_manager)/launch/generated/" + str(sim_name) + "/robots_$(arg type).launch\"/>\n\
\n\
</launch>\n\
\n")
            f.close()
            print("../launch/generated/" + sim_name + "/generated_main_" + sim_name + ".launch" + " created")
            return
