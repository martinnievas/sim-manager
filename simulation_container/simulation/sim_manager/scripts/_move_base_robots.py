import os
import platform
import sys
import argparse
import shutil
import csv

def create_robots_move_base_config(sim_name, robot_count):

    try:
        os.makedirs("../config/generated/" + sim_name)
    except FileExistsError:
        shutil.rmtree("../config/generated/" + sim_name)
        os.makedirs("../config/generated/" + sim_name)
    except FileNotFoundError:
        os.makedirs("../config/generated/" + sim_name)
    for robot_id in range(0, int(robot_count)):
        #--------------------create costmap common param file--------------------
        create_costmap_common_params_file(sim_name, robot_id)

        #--------------------create local costmap common param file--------------------
        create_local_costmap_common_param_file(sim_name, robot_id)

        #--------------------create global costmap common param file--------------------
        create_global_costmap_common_param_file(sim_name, robot_id)

        #--------------------create base local planner param file--------------------
        create_base_local_planner_param_file(sim_name, robot_id)

    return True

def create_robots_move_base_config_from_dict(tomli_dict):

    sim_name = tomli_dict['simulation']['sim_name']

    try:
        os.makedirs("../config/generated/" + sim_name)
    except FileExistsError:
        shutil.rmtree("../config/generated/" + sim_name)
        os.makedirs("../config/generated/" + sim_name)
    except FileNotFoundError:
        os.makedirs("../config/generated/" + sim_name)


    for item in tomli_dict:
        if tomli_dict[item]['type'] == "simple robot" or tomli_dict[item]['type'] == "romaa":
            robot_id = str(tomli_dict[item]['robotid'])
            #--------------------create costmap common param file--------------------
            create_costmap_common_params_file(sim_name, robot_id)

            #--------------------create local costmap common param file--------------------
            create_local_costmap_common_param_file(sim_name, robot_id)

            #--------------------create global costmap common param file--------------------
            create_global_costmap_common_param_file(sim_name, robot_id)

            #--------------------create base local planner param file--------------------
            create_base_local_planner_param_file(sim_name, robot_id)

    return True

def create_costmap_common_params_file(sim_name, robot_id):
    f = open("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_costmap_common_params.yaml", "w+")
    f.write("obstacle_range: 2.5\n\
raytrace_range: 3.0\n\
#footprint: [[x0, y0], [x1, y1], ... [xn, yn]]\n\
robot_radius: 0.3\n\
inflation_radius: 0.3\n\
\n\
observation_sources: laser_scan_sensor\n\
\n\
laser_scan_sensor: {sensor_frame: robot" + str(robot_id) + "_tf/sensor_laser, data_type: LaserScan, topic: /robot" + str(robot_id) + "/laser_scan, marking: true, clearing: true}\n\
\n")
    print("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_costmap_common_params.yaml" + " created")
    f.close()

def create_local_costmap_common_param_file(sim_name, robot_id):
    f = open("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_local_costmap_params.yaml", "w+")
    f.write("local_costmap:\n\
  global_frame: robot" + str(robot_id) + "_tf/odom\n\
  robot_base_frame: robot" + str(robot_id) + "_tf/base_link\n\
  update_frequency: 1.0\n\
  publish_frequency: 1.0\n\
  static_map: true\n\
  rolling_window: true\n\
  width: 5.0\n\
  height: 5.0\n\
  resolution: 0.05\n\
\n")
    print("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_local_costmap_params.yaml" + " created")
    f.close()


def create_global_costmap_common_param_file(sim_name, robot_id):
    f = open("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_global_costmap_params.yaml", "w+")
    f.write("global_costmap:\n\
  global_frame: robot" + str(robot_id) +"_tf/odom\n\
  robot_base_frame: robot" + str(robot_id) +"_tf/base_link\n\
  update_frequency: 1.0\n\
  static_map: true\n\
\n")
    print("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_global_costmap_params.yaml" + " created")
    f.close()


def create_base_local_planner_param_file(sim_name, robot_id):
    f = open("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_base_local_planner_params.yaml", "w+")
    f.write("TrajectoryPlannerROS:\n\
  max_vel_x: 0.4\n\
  min_vel_x: 0.05\n\
  max_vel_theta: 1.5\n\
  min_in_place_vel_theta: 0.5\n\
\n\
  acc_lim_theta: 3.2\n\
  acc_lim_x: 2.5\n\
  acc_lim_y: 2.5\n\
\n\
  holonomic_robot: true\n\
\n")
    print("../config/generated/" + sim_name + "/robot" + str(robot_id) + "_base_local_planner_params.yaml" + " created")
    f.close()
